import bean.config.task2.ConfigurationBean2;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class App {

  public static void main(String[] args) {
    ApplicationContext applicationContext = new AnnotationConfigApplicationContext(
        ConfigurationBean2.class);
    for (String beanName : applicationContext.getBeanDefinitionNames()) {
      System.out.println(applicationContext.getBean(beanName).getClass().toString());
    }
  }
}
