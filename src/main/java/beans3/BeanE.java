package beans3;

public class BeanE implements java.io.Serializable {

  private String name;
  private int value;

  public BeanE() {
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getValue() {
    return value;
  }

  public void setValue(int value) {
    this.value = value;
  }
}
