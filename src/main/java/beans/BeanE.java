package beans;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import validator.Validator;

public class BeanE implements java.io.Serializable, Validator {

  private int value;
  private String name;
  private static Logger logger = LogManager.getLogger();

  public BeanE() {

  }

  public BeanE(BeanA beanA) {
    this.name = beanA.getName();
    this.value = beanA.getValue();
  }

  public int getValue() {
    return value;
  }

  public void setValue(int value) {
    this.value = value;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @PostConstruct
  private void construct() {
    logger.warn("BeanE is constructed");
  }

  @PreDestroy
  private void destroy() {
    logger.info("BeanE destroyed");
  }

  @Override
  public String toString() {
    return "name: " + name + " value: " + value;
  }

  @Override
  public void validate() {
    if (name != null && value > 0) {
      logger.info("validated");
    } else {
      logger.info("not validated");
    }
  }
}
