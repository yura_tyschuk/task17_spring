package beans;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import validator.Validator;

public class BeanC implements java.io.Serializable, Validator {

  private int value;
  private String name;
  private static Logger logger = LogManager.getLogger();

  public BeanC() {

  }

  public BeanC(int value, String name) {
    this.value = value;
    this.name = name;
  }

  public int getValue() {
    return value;
  }

  public void setValue(int value) {
    this.value = value;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  private void init() {
    logger.info("BeanC init");
  }


  private void destroy() {
    logger.info("BeanC closed");
  }

  @Override
  public String toString() {
    return "name: " + name + " value: " + value;
  }


  @Override
  public void validate() {
    if (name != null && value > 0) {
      logger.info("validated");
    } else {
      logger.info("not validated");
    }
  }
}
