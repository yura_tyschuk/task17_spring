package beans;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.BeanPostProcessor;

public class BeanPostProcess implements BeanPostProcessor {

  private String name;
  private static Logger logger = LogManager.getLogger();

  @Value("Post")
  public void setName(String name) {
    this.name = name;
    logger.info("setName" + name);
  }

  public BeanPostProcess() {
    logger.info("Post process constructor");
  }

  @Override
  public Object postProcessBeforeInitialization(Object bean, String beanName) {
    System.out.println("postProcessBeforeInitialization()");
    System.out.println("   >>bean=" + bean + " beanName=" + beanName);
    return bean;
  }

  @Override
  public Object postProcessAfterInitialization(Object bean, String beanName) {
    System.out.println("postProcessAfterInitialization()");
    System.out.println("   >>bean=" + bean + " beanName=" + beanName);
    return bean;
  }
}



