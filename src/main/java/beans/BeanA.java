package beans;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import validator.Validator;

public class BeanA implements java.io.Serializable, Validator, InitializingBean {

  private int value;
  private String name;
  private static Logger logger = LogManager.getLogger();

  public BeanA() {

  }

  public BeanA(BeanC beanC, BeanD beanD) {
    this.name = beanC.getName();
    this.value = beanD.getValue();
  }

  public BeanA(BeanB beanB, BeanC beanC) {
    this.name = beanB.getName();
    this.value = beanC.getValue();
  }

  public BeanA(BeanB beanB, BeanD beanD) {
    this.name = beanB.getName();
    this.value = beanD.getValue();
  }

  public int getValue() {
    return value;
  }

  public void setValue(int value) {
    this.value = value;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public String toString() {
    return "name: " + name + " value: " + value;
  }

  @Override
  public void validate() {
    if (name != null && value > 0) {
      logger.info("validated");
    } else {
      logger.info("not validated");
    }
  }

  @Override
  public void afterPropertiesSet() throws Exception {
    logger.info("After properties set");
  }
}
