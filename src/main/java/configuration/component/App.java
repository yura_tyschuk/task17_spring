package configuration.component;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class App {

  private static Logger logger = LogManager.getLogger();

  public static void main(String[] args) {
    AnnotationConfigApplicationContext context =
        new AnnotationConfigApplicationContext();
    context.getEnvironment().setActiveProfiles("Persian");
    context.register(Configuration.class);
    context.refresh();

    logger.info(context.getBean(MyCat.class));
  }
}
