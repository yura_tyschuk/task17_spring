package configuration.component;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile("Persian")
public class PersianCat extends Cat {

  public PersianCat() {
    catName = "Dima";
    catAge = 4;
  }

  @Override
  public String toString() {
    return "Cat name: " + catName + " cat age: " + catAge;
  }
}
