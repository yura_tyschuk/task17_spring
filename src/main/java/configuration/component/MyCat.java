package configuration.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MyCat {

  @Autowired
  private Cat cat;

  @Override
  public String toString() {
    return cat.toString();
  }
}
