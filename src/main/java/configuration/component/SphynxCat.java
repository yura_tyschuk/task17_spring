package configuration.component;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile("Sphynx")
public class SphynxCat extends Cat {

  public SphynxCat() {
    catName = "Anton";
    catAge = 5;
  }

  @Override
  public String toString() {
    return "Cat name = " + catName + " cat age: " + catAge;
  }
}
