package configuration.bean;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Profile;

@org.springframework.context.annotation.Configuration
@ComponentScan
public class Configuration {

  @Bean
  @Profile("Sphynx")
  public Cat getCatSphynx() {
    String catName = "Anton";
    int catAge = 5;

    return new Cat(catName, catAge);
  }

  @Bean
  @Profile("Persian")
  public Cat getCatPersian() {
    String catName = "Dima";
    int catAge = 4;

    return new Cat(catName, catAge);
  }
}
