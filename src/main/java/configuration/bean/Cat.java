package configuration.bean;

public class Cat {

  private String catName;
  private int catAge;

  public Cat(String catName, int catAge) {
    this.catName = catName;
    this.catAge = catAge;
  }

  @Override
  public String toString() {
    return "Cat name: " + catName + " cat age: " + catAge;
  }
}
