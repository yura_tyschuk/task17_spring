package bean.config.task2;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;

@Configuration
@ComponentScan(basePackages = "beans2",
    includeFilters = @Filter(type = FilterType.REGEX, pattern = "beans2.[NR].*"))
@ComponentScan(basePackages = "beans3",
    includeFilters = @Filter(type = FilterType.ASSIGNABLE_TYPE,
        classes = {beans3.BeanD.class, beans3.BeanF.class}))
public class ConfigurationBean2 {

}
