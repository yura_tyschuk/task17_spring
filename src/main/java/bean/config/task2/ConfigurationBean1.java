package bean.config.task2;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("beans1")
public class ConfigurationBean1 {

}
