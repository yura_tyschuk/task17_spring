package bean.config.task1;

import beans.BeanF;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

@Configuration
public class BeanConfigF {

  @Bean(name = "BeanF", initMethod = "init")
  @Lazy
  public BeanF getBeanF() {
    return new BeanF();
  }


}
