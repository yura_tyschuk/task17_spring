package bean.config.task1;

import beans.BeanA;
import beans.BeanB;
import beans.BeanC;
import beans.BeanD;
import beans.BeanE;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("my.properties")
@Import(BeanConfigF.class)
public class BeanConfig {

  @Value("${beanB.name}")
  private String nameB;
  @Value("${beanB.value}")
  private int valueB;

  @Value("${beanC.name}")
  private String nameC;
  @Value("${beanC.value}")
  private int valueC;

  @Value("${beanD.name}")
  private String nameD;
  @Value("${beanD.value}")
  private int valueD;

  @Bean("BeanA")
  public BeanA getBeanA(BeanC getBeanC, BeanD getBeanD) {
    return new BeanA(getBeanC, getBeanD);
  }

  @Bean("BeanA")
  public BeanA getBeanA2(BeanB getBeanB, BeanC getBeanC) {
    return new BeanA(getBeanB, getBeanC);
  }

  @Bean("BeanA")
  public BeanA getBeanA3(BeanB getBeanB, BeanD getBeanD) {
    return new BeanA(getBeanB, getBeanD);
  }


  @Bean(name = "BeanB", initMethod = "init", destroyMethod = "destroy")
  public BeanB getBeanB() {
    return new BeanB(nameB, valueB);
  }

  @Bean(name = "BeanC", initMethod = "init", destroyMethod = "destroy")
  @DependsOn(value = {"BeanD", "BeanB"})
  public BeanC getBeanC() {
    return new BeanC(valueC, nameC);
  }

  @Bean(name="BeanD", initMethod = "init", destroyMethod = "destroy")
  public BeanD getBeanD() {
    return new BeanD(valueD, nameD);
  }

  @Bean("BeanE")
  public BeanE getBeanE(BeanA getBeanA) {
    return new BeanE(getBeanA);
  }

  @Bean("BeanE")
  public BeanE getBeanE2(BeanA getBeanA2) {
    return new BeanE(getBeanA2);
  }

  @Bean("BeanE")
  public BeanE getBeanE3(BeanA getBeanA3) {
    return new BeanE(getBeanA3);
  }




}
