package beans2;

public class RoseFlower implements java.io.Serializable{

  private String roseFlowerType;
  private int count;

  public RoseFlower() {
  }

  public String getRoseFlowerType() {
    return roseFlowerType;
  }

  public void setRoseFlowerType(String roseFlowerType) {
    this.roseFlowerType = roseFlowerType;
  }

  public int getCount() {
    return count;
  }

  public void setCount(int count) {
    this.count = count;
  }
}
