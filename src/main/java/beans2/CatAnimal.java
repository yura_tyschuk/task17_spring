package beans2;

public class CatAnimal implements java.io.Serializable{

  private String catType;
  private int count;

  public CatAnimal() {

  }

  public String getCatType() {
    return catType;
  }

  public void setCatType(String catType) {
    this.catType = catType;
  }

  public int getCount() {
    return count;
  }

  public void setCount(int count) {
    this.count = count;
  }
}
