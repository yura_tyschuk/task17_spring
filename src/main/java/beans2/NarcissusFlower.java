package beans2;

public class NarcissusFlower implements java.io.Serializable {

  private String flowerType;
  private int count;

  public NarcissusFlower() {

  }

  public String getFlowerType() {
    return flowerType;
  }

  public void setFlowerType(String flowerType) {
    this.flowerType = flowerType;
  }

  public int getCount() {
    return count;
  }

  public void setCount(int count) {
    this.count = count;
  }
}
