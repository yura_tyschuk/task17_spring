package other.beans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BeanSetterBased {

  private OtherBeanA otherBeanA;
  private OtherBeanB otherBeanB;
  private OtherBeanC otherBeanC;

  @Autowired
  public void setOtherBeanA(OtherBeanA otherBeanA) {
    this.otherBeanA = otherBeanA;
  }

  @Autowired
  public void setOtherBeanB(OtherBeanB otherBeanB) {
    this.otherBeanB = otherBeanB;
  }

  @Autowired
  public void setOtherBeanC(OtherBeanC otherBeanC) {
    this.otherBeanC = otherBeanC;
  }

  @Override
  public String toString() {
    return "Setter based: otherBeanA: " + otherBeanA + " otherBeanB: " +
        otherBeanB + " otherBeanC: " + otherBeanC;
  }
}
