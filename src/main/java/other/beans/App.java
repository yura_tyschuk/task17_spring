package other.beans;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class App {

  private static Logger logger = LogManager.getLogger();

  public static void main(String[] args) {
    ApplicationContext applicationContext =
        new AnnotationConfigApplicationContext(Config.class);

    logger.info(applicationContext.getBean(OtherBeanA.class));
    logger.info(applicationContext.getBean(OtherBeanA.class));

    logger.info(applicationContext.getBean(OtherBeanB.class));
    logger.info(applicationContext.getBean(OtherBeanB.class));

  }
}
