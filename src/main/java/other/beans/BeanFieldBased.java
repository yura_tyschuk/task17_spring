package other.beans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class BeanFieldBased {

  @Autowired
  private OtherBeanA otherBeanA;
  @Autowired
  @Qualifier("myBean")
  private OtherBeanB otherBeanB;
  @Autowired
  private OtherBeanC otherBeanC;

  @Override
  public String toString() {
    return "Field based: otherBeanA: " + otherBeanA + " otherBeanB: " +
        otherBeanB + " otherBeanC: " + otherBeanC;
  }
}
