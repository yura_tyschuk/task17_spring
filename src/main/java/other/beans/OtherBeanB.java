package other.beans;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Qualifier("myBean")
@Scope("prototype")
public class OtherBeanB {

}
