package other.beans;

import org.springframework.stereotype.Component;

@Component
public class BeanConstructorBased {

  private OtherBeanA otherBeanA;
  private OtherBeanB otherBeanB;
  private OtherBeanC otherBeanC;

  public BeanConstructorBased(OtherBeanA otherBeanA, OtherBeanB otherBeanB,
      OtherBeanC otherBeanC) {
    this.otherBeanA = otherBeanA;
    this.otherBeanB = otherBeanB;
    this.otherBeanC = otherBeanC;

  }

  @Override
  public String toString() {
    return "Constructor based: otherBeanA: " + otherBeanA + " otherBeanB: " +
        otherBeanB + " otherBeanC: " + otherBeanC;
  }
}
