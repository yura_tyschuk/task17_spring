package beans1;

import org.springframework.stereotype.Component;

@Component
public class BeanA implements java.io.Serializable {

  private String name;
  private int value;

  public BeanA() {
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getValue() {
    return value;
  }

  public void setValue(int value) {
    this.value = value;
  }
}
