package collections.injection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class BeanForPrimaryAndQualifier {

  @Autowired
  Computer baseBean;
  @Autowired
  @Qualifier("Lenovo")
  Computer baseBean2;
  @Autowired
  @Qualifier("Toshiba")
  Computer baseBean3;

  @Override
  public String toString() {
    return "baseBean = " + baseBean + " baseBean2 = " + baseBean2 +
        "baseBean3 = " + baseBean3;
  }

}
