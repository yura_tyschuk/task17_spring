package collections.injection;

import org.apache.logging.log4j.core.config.Order;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Component
@Order(1)
@Primary
public class Asus implements Computer {

  @Override
  public String getComputer() {
    return "Asus";
  }
}
