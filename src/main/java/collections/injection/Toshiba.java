package collections.injection;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(2)
@Qualifier("Toshiba")
public class Toshiba implements Computer {

  @Override
  public String getComputer() {
    return "Toshiba";
  }
}
