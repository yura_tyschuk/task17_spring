package collections.injection;

import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GeneralComputerBean {

  @Autowired
  private List<Computer> computerList;
  private static Logger logger = LogManager.getLogger();

  public void printComputers() {
    for (Computer computer : computerList) {
      logger.info(computer.getComputer());
    }
  }
}
