package collections.injection;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(Ordered.LOWEST_PRECEDENCE)
@Qualifier("Lenovo")
public class Lenovo implements Computer {

  @Override
  public String getComputer() {
    return "Lenovo";
  }
}
