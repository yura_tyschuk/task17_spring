package collections.injection;

public interface Computer {

  String getComputer();
}
